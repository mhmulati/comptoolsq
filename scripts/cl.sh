#!/usr/bin/env bash

if [ ! -f CMakeLists.txt ]
then
	echo "Error: File CMakeLists.txt does not exist"
	exit 2
fi

# rm bin -rfv
rm lib -rfv
rm build -rfv
rm cmake-build-release -rfv
rm cmake-build-debug -rfv
rm cmake-build-relwithdebinfo -rfv
rm cmake-build-minsizerel -rfv
rm CMakeCache -rfv
rm CMakeCache.txt -fv
rm CMakeFiles -rfv
