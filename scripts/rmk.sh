#!/usr/bin/env bash

CMAKE_BUILD_TYPE="DebugLog"  # CMAKE_BUILD_TYPE="None"
CMAKE_INSTALL_PREFIX="~"

if [ $# -ne 0 -a $# -ne 1 -a $# -ne 2 ]
then
   echo "Error: $# is not the correct number of arguments"
   echo "Usage: $(basename ${0}) [ [ None | Debug | Release | RelWithDebInfo | MinSizeRel | ... ] CMAKE_INSTALL_PREFIX ]"
   echo "CMAKE_BUILD_TYPE default is ${CMAKE_BUILD_TYPE}"
   echo "CMAKE_INSTALL_PREFIX default is ${CMAKE_INSTALL_PREFIX}"
   exit 2
fi

if [ $# = 1 ]
then
	CMAKE_BUILD_TYPE=${1}
fi

if [ $# = 2 ]
then
	CMAKE_INSTALL_PREFIX=${2}
fi

cl && gn ${CMAKE_BUILD_TYPE} ${CMAKE_INSTALL_PREFIX} && mk
