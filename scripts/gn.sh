#!/usr/bin/env bash

# https://tuannguyen68.gitbooks.io/learning-cmake-a-beginner-s-guide/content/chap1/chap1.html
#cmake -H. -Bbuild

CMAKE_BUILD_TYPE="Debug"
# CMAKE_BUILD_TYPE="None"
CMAKE_INSTALL_PREFIX="~"

if [ $# -ne 0 -a $# -ne 1 -a $# -ne 2 ]; then
   echo "Error: $# is not the correct number of arguments"
   echo "Usage: $(basename ${0}) [ [ None | Debug | Release | RelWithDebInfo | MinSizeRel | ... ] CMAKE_INSTALL_PREFIX ]"
   echo "CMAKE_BUILD_TYPE default is ${CMAKE_BUILD_TYPE}"
   echo "CMAKE_INSTALL_PREFIX default is ${CMAKE_INSTALL_PREFIX}"
   exit 2
fi

if [ $# -ge 1 ]; then
	CMAKE_BUILD_TYPE=${1}
fi

if [ $# -ge 2 ]; then
	CMAKE_INSTALL_PREFIX=${2}
fi

# CMAKE_BUILD_TYPES_LIST="None Debug Release RelWithDebInfo MinSizeRel"

# if [ -z "`echo ${CMAKE_BUILD_TYPES_LIST} | grep ${CMAKE_BUILD_TYPE}`" ]
# then
# 	echo "Error: Invalid CMAKE_BUILD_TYPE value"
# 	exit 2
# fi

cmake -H. -Bbuild -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DCMAKE_INSTALL_PREFIX=${CMAKE_INSTALL_PREFIX}
